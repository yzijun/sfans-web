package org.sfans.core.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class ResourceConflictException extends RuntimeException {

	private static final long serialVersionUID = -6087648323218652262L;

	public ResourceConflictException() {
	}

	public ResourceConflictException(final String message) {
		super(message);
	}
}
