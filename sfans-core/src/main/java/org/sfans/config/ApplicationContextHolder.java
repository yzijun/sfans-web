package org.sfans.config;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ApplicationContextHolder implements ApplicationContextAware {

	private static ApplicationContext context;

	private ApplicationContextHolder() {}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		context = applicationContext;
	}


	public static <T> T getBean(String beanName, Class<T> clazz){
		if (beanName == null) return null;
		return context.getBean(beanName, clazz);
	}

	public static ApplicationContext getContext(){
		if (context == null) return null;
		return context;
	}
}
